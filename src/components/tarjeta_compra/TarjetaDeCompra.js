import React from 'react';
// import '../css/TarjetaDeCompra.css' // -importar de manera convencional

import styles from '../tarjeta_compra/css/TarjetaDeCompra.module.css'

class TarjetaFruta extends React.Component {
    constructor(){
        super()
        this.state = {
            cantidad:0
        }
        //this.ChangeStatePlus = this.ChangeStatePlus.bind(this) // enlazar el this del metodo hacia el this de la clase
        //this.ChangeStateSubstract = this.ChangeStateSubstract.bind(this)
        const METHODS = [
            'ChangeStatePlus',
            'ChangeStateSubstract',
            'clean'
        ]
        METHODS.forEach((method)=>{
            this[method] = this[method].bind(this)
        })
    }
    ChangeStatePlus(){
        this.setState({cantidad :this.state.cantidad + 1 })
    }
    ChangeStateSubstract(){
        this.setState({cantidad :this.state.cantidad - 1 })
    }
    clean(){
        this.setState({cantidad :this.state.cantidad - this.state.cantidad })
    }

     
    
    render(){
        const hasItems = this.state.cantidad > 0;
        const activeClass = hasItems ? styles['sale-card-active'] : '';
        const clases = styles['sale-card'] + ' ' +  activeClass;
        return (
            <div className={clases}>
            <h3>TITULO</h3>
            <hr/>
            <p>producto :  {this.props.name}</p>
            <p>valor  $ : {this.props.price}</p>
            <div>cantidad : {this.state.cantidad}</div>
            <p>
                Total precio : $ { this.props.price * this.state.cantidad}
            </p>
            <button onClick={ this.ChangeStatePlus   }> + </button>
            <button onClick={ this.ChangeStateSubstract   }> - </button>
            <button onClick={ this.clean   }> Reset </button>
        </div>
        )
    }


}
export default TarjetaFruta;