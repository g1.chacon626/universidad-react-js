import React from 'react';

class Contador extends React.Component {
    state = {
        clicks : 0,
        video: {
            title : 'Super video',
            likes : 0
        } 
    }
    add = () => {
        //la forma de actualizar el estado de manera segura es con una funcion pasando el estado previo
        // si la funcion retorna un null el estado no se actualizara
        //para no perder la propiedades del objeto actualizado se realiza un spread antes de actualizar el estado
        this.setState((prevState,props) => ({
                video : {
                    ...prevState.video,
                    likes : prevState.video.likes + 1
                }   
        }))
    }
    render (){
        return(
            <div>
                <h1>
                    { this.state.video.title }
                </h1>
                <button onClick={this.add}>
                    Likes : ({ this.state.video.likes})
                </button>
            </div>
        )
    }
}
class Gato extends React.Component {
    constructor(props){
        super()
    }
    
    render(){
        return (
            <div>
                <h1>GATO ....</h1>
                <pre>
                    {JSON.stringify(this.props , null , 4)}
                </pre>        
            </div>
        )
    }
}
const styles = {
    height : '200px',
    background : 'gold',
    padding : '1em',
    boxSizing : 'border-box'
}
 class clase32 extends React.Component {
    state = {
        x : 0 ,
        y : 0 
    }
    
    manejador = (event) => {
        this.setState({
            x : event.clientX,
            y : event.clientY
        })
    } 
    render() {
         return (
             <div style={styles}
                  onMouseMove={this.manejador}
             >
                 <div>
                     x : {this.state.x}
                 </div>
                 <div>
                     y : {this.state.y}
                 </div>
             </div>
         )
     }
 }
 class Clase33 extends React.Component {
    state = {
        text : '',
        event : ''
    } 
    handler = (event) => {
        console.log("este es el evento : " , event.target.value);
        this.setState({
            text : event.target.value,
            event : event.type
        })
     }
     render(){
         return (
             <div>
                 <input type="text" name="" id="" onChange={this.handler}/>
                 <h1>
                     {this.state.text}
                 </h1>
                 <h2>
                     {this.state.event}
                 </h2>
             </div>
         )
     }
 }
 class Clase34 extends React.Component {
     handler = (e) => {
        e.preventDefault()
        console.log(e);
        //con la instruccion .nativeEvent podemos acceder al evento nativo del navegador
        console.log(e.nativeEvent)
     }
     render(){
         return (
             <div>
                 <a href="https://google.com"
                    onClick={this.handler}
                 >
                     GOOGLE
                 </a>
             </div>
         )
     }
 }
 class Clase35 extends React.Component {
    state = {
        color : "blue"
    }
    
    handlerChange= (event) => {
        console.log(event.target.value);
        let color = event.target.value;
        this.setState({
            color : color
        })
    }
     render(){
         return (
             <div>
                 <input type="text" name="" id="" onChange={this.handlerChange}/>
                 <h1 style={{color : this.state.color}}>
                     {this.state.color}
                 </h1>
             </div>
         )
     }
 }
 class Clase36Hijo extends React.Component {
    
    state = {
        name : '',
        age : 0,
        work : ''
    }
    
    manejadorClick = () => {
        //let propName = this.state.name;
        this.props.onSaluda(this.state);
    }
    manejadorInput = (event) => {
        let key_input = event.target.id;
        let key_value  = event.target.value;
        this.setState((prevState) => {
            //console.log("estado previo : " , prevState);
            if(key_input == "name"){
                return {
                    name  : key_value
                }
            }else if (key_input == "age"){
                return {
                    age : key_value
                }
            }else if (key_input == "work"){
                return {
                    work : key_value
                }
            }
        })
        
    } 
    
    render(){
        //console.log("nuevo estado  : " , this.state);
         return (
             <div className="box blue">
                 <h2>
                     Componente hijo
                 </h2>
                 <label htmlFor="">Nombre</label>
                 <input type="text" name="" id="name"   onChange={this.manejadorInput}/>

                 <label htmlFor="">Edad</label >
                 <input type="tel" name="" id="age"     onChange={this.manejadorInput}/>

                 <label htmlFor="">Profesion</label>
                 <input type="text" name="" id="work"   onChange={this.manejadorInput}/>

                 <button onClick={this.manejadorClick}>
                     Enviar
                 </button>
             </div>
         )
     }
 }
 class Clase37 extends React.Component {
    render(){

        return (
            <div>
                <div> 
                    {this.props.name && <strong> {this.props.name} </strong>}
                </div>
                {this.props.saluda              ? 
                    (<h2> Hola carlos </h2>)    : 
                    (<h2>Wops... no hay saludo</h2>)
                }
            </div>
            
        )

        /*if(this.props.saluda){
            return(
                <div>
                    <h3>
                        Hola carlos chacon
                    </h3>
                </div>
            )
        }
        return(
            <div>
                <h3>
                    woops... no hay saludo
                </h3>
            </div>
        )*/
        
    }

 }
 const  Clase39 = ({ uiColor , children }) => {
    
        const styles = {
            padding : '.3em',
            color : '#fff',
            background : uiColor,
            borderRadius : '.3em',
            textAlign : 'center',
            fontSize : '50px'
        }
        return(
            <h1 style={ styles }>
                {children}
            </h1>
        )
    

 }
export default Clase39;