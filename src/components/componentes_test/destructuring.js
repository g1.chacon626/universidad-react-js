const USER1 = {
    name : 'Carlos Fernando Chacon',
    username : 'KYUTA-SAN',
    country : 'Colombia',
    social : {
        facebook : 'https://facebook.com',
        twitter : 'https://twitter.com'
    }

}

const Saluda = ( user )  => {
    const { name , social : { twitter , facebook } , country } = user;
    const ordeComidas = ['pizza' , 'te verde' , 'pay' , 'banana' , 'proteina'];
    const [ comida , bebida , postre , ...restantes] = ordeComidas;

    console.log(
        restantes , 
    `Hola soy ${name} y vivo en ${country} mi twitter es ${twitter} y mi facebook es ${facebook}.
    Mi comida favorita es ${comida} mi bebida ${bebida} y mi postre ${postre}`
    );

}
Saluda(USER1);