import React from 'react';
import logo from './logo.svg';
import './App.css';
import TarjetaFruta from './components/tarjeta_compra'
import TestComponent from './components/componentes_test'
import './components/componentes_test/destructuring'

class  App extends React.Component {
    state = {
      name :'',
      age : 0,
      work : '',
      uiColor : 'red'
    }
  
  
  Manejador = (infoChild) =>{
    //alert('Hey ' + name);
    this.setState({...infoChild})
  };

  render(){
    // const state  = {
    //   fuerza : 100,
    //   vidasRestantes : 7
    // }
  
    // const otrosDatos = {
    //   raza : "tropical",
    //   peleasNocturnas : 300
    // }
    const { uiColor } = this.state;
    return (
      //  <div> <TarjetaFruta name='Figura one piece' price={200.000} /> </div>
      //div para clase 36
      /*<div className="box red"> 
        <h1> Componente padre </h1>
          <h2>
            nombre : {this.state.name}
            <br/>
            edad : {this.state.age}
            <br/>
            profesion : {this.state.work}
          </h2>
        <TestComponent
          //props->
          names='garfield'
          edad='2 años'
          {...otrosDatos}
          {...state}
          //-->evento
          onSaluda={ this.Manejador }
        /> 
      </div>*/
      <div>
        <TestComponent saluda={true} name={'carlos'} uiColor={ uiColor }>
          Carlos Chacon <em> Programador </em>
        </TestComponent>
      </div>
    );
  }
  
}

export default App;
