import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//componente funcional
// const TarjetaFruta = (props) => {
//     return(
//         <div>
//             <h3>TITULO</h3>
//             <hr/>
//             <p>mi nombre es :  {props.name}</p>
//             <p>Mi edad es de : {props.age}</p>
//         </div>
//     )
// }



ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
